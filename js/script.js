// calculator
function calculate() {
  var num1 = parseFloat(document.getElementById('num1').value);
  var num2 = parseFloat(document.getElementById('num2').value);
  var operation = document.getElementById('operation').value;
  var result;

  switch (operation) {
    case 'add':
      result = num1 + num2;
      break;
    case 'subtract':
      result = num1 - num2;
      break;
    case 'multiply':
      result = num1 * num2;
      break;
    case 'divide':
      result = num1 / num2;
      break;
    default:
      result = "Operação inválida";
  }

  document.getElementById('result').value = result;
}

// temperature_converter
function convertTemperature() {
  var celsius = parseFloat(document.getElementById('celsius').value);
  var fahrenheit = parseFloat(document.getElementById('fahrenheit').value);
  var kelvin = parseFloat(document.getElementById('kelvin').value);

  if (!isNaN(celsius)) {
    // Se a entrada for em Celsius
    var fahrenheitFromCelsius = (celsius * 9 / 5) + 32;
    var kelvinFromCelsius = celsius + 273.15;

    document.getElementById('fahrenheit').value = fahrenheitFromCelsius.toFixed(2);
    document.getElementById('kelvin').value = kelvinFromCelsius.toFixed(2);
  } else if (!isNaN(fahrenheit)) {
    // Se a entrada for em Fahrenheit
    var celsiusFromFahrenheit = (fahrenheit - 32) * 5 / 9;
    var kelvinFromFahrenheit = (fahrenheit + 459.67) * 5 / 9;

    document.getElementById('celsius').value = celsiusFromFahrenheit.toFixed(2);
    document.getElementById('kelvin').value = kelvinFromFahrenheit.toFixed(2);
  } else if (!isNaN(kelvin)) {
    // Se a entrada for em Kelvin
    var celsiusFromKelvin = kelvin - 273.15;
    var fahrenheitFromKelvin = (kelvin * 9 / 5) - 459.67;

    document.getElementById('celsius').value = celsiusFromKelvin.toFixed(2);
    document.getElementById('fahrenheit').value = fahrenheitFromKelvin.toFixed(2);
  }
}

// Limpeza dos campos (mantida a mesma função clearFields)
function clearFields(excludeField) {
  var fields = ['celsius', 'fahrenheit', 'kelvin'];
  fields.forEach(function (field) {
    if (field !== excludeField) {
      document.getElementById(field).value = '';
    }
  });
}
